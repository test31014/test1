import Search1 from '../components/search'
import Side from '../components/side'
import { FiUserPlus } from 'react-icons/fi'
import { FiUserX } from 'react-icons/fi'
import { FiUserCheck } from 'react-icons/fi'
import { FiCreditCard } from 'react-icons/fi'
import { FiAward } from 'react-icons/fi'
import { FiClipboard } from 'react-icons/fi'
import { display } from '@mui/system'
import { useState } from 'react'
import './employee.css'
import Emp_model from './../components/addemp'
import Remove_emp from './../components/modelas/removeemp'
import Edit_emp from '../components/modelas/edit_emp'
const Employee = () => {
  const [emp_mo, setempm] = useState(false)
  const [emp_re, setempre] = useState(false)
  const [emp_ed, setemped] = useState(false)
  return (
    <div style={{ display: 'flex' }}>
      <div>
        <Side />
      </div>

      <div>
        <div>{emp_mo && <Emp_model setopen={setempm} />}</div>
        <div>{emp_re && <Remove_emp setopen={setempre} />}</div>
        <div>{emp_ed && <Edit_emp setopen={setemped} />}</div>
        <div style={style.search}>
          <Search1 />
        </div>

        <div>
          <div style={{ display: 'flex', marginLeft: '70px' }}>
            <div
              style={style.icon}
              className='box1'
              onClick={() => {
                setempm(true)
              }}>
              <FiUserPlus size={70} />
              <p>Add Employee</p>
            </div>
            <div
              style={style.icon}
              className='box1'
              onClick={() => {
                setempre(true)
              }}>
              <FiUserX size={70} />
              <p>Remove Employee</p>
            </div>
            <div
              style={style.icon}
              className='box1'
              onClick={() => {
                setemped(true)
              }}>
              <FiUserCheck size={70} />
              <p>Edit Employee</p>
            </div>
            <div style={style.icon} className='box1'>
              <FiCreditCard size={70} />
              <p>Leave Applications</p>
            </div>
            <div style={style.icon} className='box1'>
              <FiAward size={70} />
              <p>Appraisal</p>
            </div>
            <div style={style.icon} className='box1'>
              <FiClipboard size={70} />
              <p>Feedback</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const style = {
  search: {
    width: '950px',
    height: '400px',
    background: 'white',
    marginLeft: '110px',
    marginTop: '40px',
    overflow: 'hidden',
    overflowY: 'scroll',
    backgroundImage: 'url("searchback.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    padding: '20px',
  },
  icon: {
    marginLeft: '60px',
    marginTop: '60px',
    textAlign: 'center',
  },
}

export default Employee
