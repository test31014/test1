import '../App.css'
import 'flip-card-wc'
import Head from '../components/head'
import { useState } from 'react'
import Model from './../components/model'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
const Login = () => {
  const [openmodel, setopen] = useState(false)
  const [email, setemail] = useState('')
  const [password, setpass] = useState('')
  const navigate = useNavigate()

  const loginadmin = () => {
    if (email.length === 0) {
      toast.error('Please Enter Your Email')
    } else if (password.length === 0) {
      toast.error('Please Enter Your Password')
    } else {
      axios
        .post('http://localhost:4000/login', { email, password })
        .then((response) => {
          const result = {}
          console.log(response)
          if (response.data.status == 'error') {
            toast.error(response.data.error)
          } else {
            sessionStorage['token'] = response.data.token
            console.log(sessionStorage['token'])
            toast.success('Welcome Admin')
            navigate('/admin')
          }
        })
    }
  }

  return (
    <div style={styles.outbox}>
      <div>{openmodel && <Model setopen={setopen} />}</div>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '900px', padding: '0' }}>
          <b style={styles.p2}>Welcome To Sunbeam Employee Portal</b>
          <br></br>
          <b style={styles.p3}>
            {' '}
            Maximise employee engagement, boost their productivity and enable
            their success.
          </b>
        </div>
        <div style={{ marginLeft: '450px' }}>
          <button
            className='btn btn-info'
            onClick={() => {
              setopen(!openmodel)
            }}>
            help
          </button>
          <button className='btn btn-primary' style={{ marginLeft: '10px' }}>
            Info
          </button>
        </div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={styles.box}>
          <flip-card
            variant='hover'
            class='trivia-card'
            style={{ height: '800px' }}>
            <div slot='front' style={styles.frontdiv}>
              <img src='admin.png' style={styles.imbox}></img>,
              <p style={styles.p1}> Login as an Admin</p>
            </div>
            <div slot='back'>
              <div
                className='mb-3'
                style={{ width: '70%', marginLeft: '10%', marginTop: '10%' }}>
                <label>Email</label>
                <input
                  className='form-control'
                  type='email'
                  onChange={(e) => {
                    setemail(e.target.value)
                  }}
                />
              </div>
              <div
                className='mb-3'
                style={{ width: '70%', marginLeft: '10%', marginTop: '12%' }}>
                <label>Password</label>
                <input
                  className='form-control'
                  type='password'
                  onChange={(e) => {
                    setpass(e.target.value)
                  }}
                />
              </div>
              <div className='mb-3' style={styles.buttondiv}>
                <button style={styles.signinButton} onClick={loginadmin}>
                  Sign-In
                </button>
              </div>
            </div>
          </flip-card>
        </div>
        <div style={styles.box2}>
          <flip-card variant='hover' class='trivia-card'>
            <div slot='front' style={styles.frontdiv}>
              {' '}
              <img src='employee.png' style={styles.imbox2}></img>
              <p style={styles.p1}>Login as an Employee</p>
            </div>
            <div slot='back'>
              <div
                className='mb-3'
                style={{ width: '70%', marginLeft: '10%', marginTop: '10%' }}>
                <label>Email</label>
                <input className='form-control' type='email' />
              </div>
              <div
                className='mb-3'
                style={{ width: '70%', marginLeft: '10%', marginTop: '10%' }}>
                <label>Password</label>
                <input className='form-control' type='password' />
              </div>
              <div className='mb-3' style={styles.buttondiv}>
                <button style={styles.signinButton}>Sign-In</button>
              </div>
            </div>
          </flip-card>
        </div>
      </div>
    </div>
  )
}

const styles = {
  box: {
    marginLeft: '37%',
    marginTop: '10%',
    borderStyle: 'none',
    borderColor: 'lightblue',
    broderWidth: '',
    width: '30%',
    height: '400px',
    padding: 20,
    background: 'none',
  },
  box2: {
    marginLeft: '0%',
    marginTop: '10%',
    borderStyle: 'none',
    borderColor: 'lightblue',
    broderWidth: '',
    width: '30%',
    height: '400px',
    padding: 20,
    background: 'none',
  },

  imbox: {
    width: '150px',
    height: '200px',
    marginTop: '30px',
  },

  imbox2: {
    width: '210px',
    height: '200px',
    marginTop: '30px',
    marginRight: '5px',
  },

  p1: {
    marginTop: '30px',
    fontFamily: 'cursive',
    backgroundColor: 'none',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '20px',
  },

  p3: {
    marginTop: '30px',
    fontFamily: 'cursive',
    background: 'none',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '18px',
  },

  p2: {
    marginTop: '30px',
    fontFamily: 'cursive',
    background: 'none',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '35px',
  },

  frontdiv: {
    textAlign: 'center',
    padding: '15px',
    background: 'white',
    opacity: '90%',
  },

  outbox: {
    backgroundImage: "url('47639.jpg') ",
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    background: 'linear-gradient(to bootom, red)',
    width: '100vw',
    height: '100vh',

    padding: '20px',
    height: '900px',
  },

  buttondiv: {
    width: '70%',
    marginLeft: '10%',
    marginTop: '20px',
  },

  signinButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#F8D90F',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 25,
  },
}

export default Login
