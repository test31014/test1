import './admin.css'
import './logout.css'
import Side from './../components/side'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import Model_info from './../components/model_edit_info'
import { display } from '@mui/system'
import { useLocation, useNavigate } from 'react-router-dom'
import Model_personal from '../components/modelas/model_personal'
const Adminpage = () => {
  const [openinfo, setoprninfo] = useState(false)
  const [openper, setper] = useState(false)
  const [admin, setadmin] = useState({})
  const [adminp, setadminp] = useState({})
  const [file, setfile] = useState()

  const navigate = useNavigate()
  const uploadimage = () => {
    const body = new FormData()
    console.log('photo uploading')
    console.log(admin.emp_id)
    body.set('photo', file)
    axios
      .post('http://localhost:4000/upload' + admin.emp_id, body, {
        headers: {
          'Content-Type': 'multipart/form-data',
          token: sessionStorage['token'],
        },
      })
      .then((response) => {
        if (response.data.status === 'success') {
          toast.success('Profile Added Sucessfully')
          navigate('/admin')
          window.location.reload()
        } else {
          toast.error('Error While Adding Image')
        }
      })
  }

  useEffect(() => {
    const loading = async () => {
      loadadmin()
      loadadminp()
    }
    loading()
  }, [])

  const loadadmin = async function () {
    await axios
      .get('http://localhost:4000/getadmin', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data
        console.log('axios get')
        console.log(response.data)
        console.log(result.status)
        if (result.status == 'success') {
          setadmin(response.data.data)
          console.log('admin value')
          console.log(admin)
        } else alert('error during getting data')
      })
  }
  const loadadminp = async function () {
    await axios
      .get('http://localhost:4000/getadminp', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data
        console.log('axios get')
        console.log(response.data)
        console.log(result.status)
        if (result.status == 'success') {
          setadminp(response.data.data)
          console.log('admin value')
          console.log(admin)
        } else alert('error during getting data')
      })
  }
  return (
    <div style={styles.adminback}>
      <div>{openinfo && <Model_info setopen={setoprninfo} />}</div>
      <div>{openper && <Model_personal setopen={setper} />}</div>
      <div>
        {' '}
        <Side />
      </div>
      <div>
        <div style={{ marginTop: '10px' }} class='container1'>
          <a href='#' class='btn'>
            Log Out
          </a>
        </div>
        <div style={styles.profile}>
          <h3 style={{ textAlign: 'left', fontFamily: 'initial' }}>
            Employee Information
          </h3>
          <div className='row'>
            <div className='col'>
              <div style={{ marginTop: '20px', display: 'flex' }}>
                <h6>Empoyee Id:</h6>
                <h6 style={styles.info}> {admin.emp_id}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Department:</h6>
                <h6 style={styles.info}> {admin.department}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Designation :</h6>
                <h6 style={styles.info}> {admin.role}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Join Date:</h6>
                <h6 style={styles.info}> {admin.join_date}</h6>
              </div>
              <div style={{ marginTop: '27px' }} class='container'>
                <a
                  href='#'
                  class='btn'
                  onClick={() => {
                    setoprninfo(true)
                  }}>
                  Edit Information
                </a>
              </div>
            </div>
            <div className='col'>
              <div style={{ marginTop: '20px', display: 'flex' }}>
                <h6>Cost To Company: Rs.</h6>
                <h6 style={styles.info}> {admin.ctc}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Email:</h6>

                <h6 style={styles.info}> {admin.email}</h6>
              </div>
              <div style={{ marginTop: '27px' }}>
                <h6>Current Project:</h6>
              </div>
              <div
                style={{
                  marginTop: '32px',
                  display: 'flex',
                }}>
                <h6>Office Location:</h6>
                <h6 style={styles.info}> {admin.office}</h6>
              </div>
            </div>
          </div>
        </div>
        <div style={styles.profile2}>
          <h3 style={{ textAlign: 'left', fontFamily: 'initial' }}>
            Personal Information
          </h3>
          <div className='row'>
            <div className='col'>
              <div style={{ marginTop: '15px', display: 'flex' }}>
                <h6>Name:</h6>
                <h6 style={styles.info2}> {adminp.name}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Date Of Birth:</h6>
                <h6 style={styles.info2}> {adminp.DOB}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Contact:</h6>
                <h6 style={styles.info2}> {adminp.contact}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Address:</h6>
                <h6 style={styles.info2}> {adminp.Address}</h6>
              </div>
              <div style={{ marginTop: '27px' }} class='container'>
                <a
                  href='#'
                  class='btn'
                  onClick={() => {
                    setper(true)
                  }}>
                  Edit Information
                </a>
              </div>
            </div>
            <div className='col' style={{ marginLeft: '30px' }}>
              <div style={{ marginTop: '20px', display: 'flex' }}>
                <h6>Emergency Contact:</h6>
                <h6 style={styles.info2}> {adminp.emergency}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Blood Group:</h6>
                <h6 style={styles.info2}> {adminp.blood}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Education:</h6>
                <h6 style={styles.info2}> {adminp.education}</h6>
              </div>
              <div style={{ marginTop: '27px', display: 'flex' }}>
                <h6>Adhar Card Id:</h6>
                <h6 style={styles.info2}> {adminp.adhar_no}</h6>
              </div>
            </div>
            <div
              className='col'
              style={{ marginLeft: '10px', display: 'flex' }}>
              <div
                className='box5'
                style={{
                  marginLeft: '130px',
                  marginRight: '20px',

                  background: 'none',
                  borderRadius: '15px',
                  overflow: 'hidden',
                }}>
                <img
                  src={'http://localhost:4000/' + admin.image}
                  style={{
                    width: '130px',
                    height: '170px',
                    borderRadius: '15px',
                  }}
                />
                <div style={{ marginTop: '8px' }}>
                  <input
                    type='file'
                    style={{ background: 'transparent', marginLeft: '15px' }}
                    onChange={(e) => {
                      setfile(e.target.files[0])
                    }}></input>
                </div>
                <div style={{ marginTop: '5px', marginLeft: '11px' }}>
                  <button
                    className='btn btn-info-btn btn-sm'
                    onClick={uploadimage}>
                    Upload Image
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div></div>
    </div>
  )
}

const styles = {
  adminback: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    margin: '0%',
    display: 'flex',
  },

  profile: {
    borderStyle: 'none',
    borderRadius: '20px',
    marginLeft: '40px',
    width: '1150px',
    hight: '400px',
    marginTop: '10px',
    backgroundImage: 'url("back3.jpg")',
    backgroundPosition: 'top',
    backgroundPosition: '0 -250px',
    backgroundRepeat: 'no-repeat',
    padding: '20px',
  },
  profile2: {
    borderStyle: 'none',
    borderRadius: '20px',
    marginLeft: '40px',
    width: '1150px',
    hight: '400px',
    marginTop: '25px',
    backgroundImage: 'url("back3.jpg")',
    backgroundPosition: 'top',
    backgroundPosition: '0 -250px',
    backgroundRepeat: 'no-repeat',
    padding: '20px',
  },
  logout: {
    borderStyle: 'none',
    marginLeft: '1140px',
    width: '70px',
    hight: '500px',
    marginTop: '5px',
  },

  info: {
    color: '#DC143C',
    marginLeft: '10px',
    fontFamily: 'cursive',
    fontWeight: 'bold',
  },
  info2: {
    color: '#581845 ',
    marginLeft: '10px',
    fontFamily: 'cursive',
    fontWeight: 'bold',
  },
}
export default Adminpage
