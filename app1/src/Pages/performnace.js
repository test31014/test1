import { Doughnut } from 'react-chartjs-2'
import ChartA from '../components/performancechart'
import ChartD from '../components/pichart'
import Side from './../components/side'

const data = {
  labels: ['Red', 'Blue', 'Black', 'Yellow', 'Pink', 'orange'],
  datasets: [
    {
      data: [12, 19, 3, 5, 2, 3],
    },
  ],
}
function Performance() {
  return (
    <div style={{ display: 'flex' }}>
      <div>
        <Side />
      </div>
      <div>
        <div style={style.chaert1}>
          <ChartD />
        </div>
        <div style={style.chaert1}>
          <ChartA />
        </div>
      </div>
    </div>
  )
}

const style = {
  chaert1: {
    marginLeft: '100px',
    borderStyle: 'none',
    height: '380px',
    marginTop: '20px',
  },
}

export default Performance
