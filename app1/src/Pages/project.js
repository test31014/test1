import Side from '../components/side'
import { FcAcceptDatabase } from 'react-icons/fc'
import { FcRefresh } from 'react-icons/fc'
import { FcMindMap } from 'react-icons/fc'
import { FcFinePrint } from 'react-icons/fc'
import { FcBiotech } from 'react-icons/fc'
import { FcFullTrash } from 'react-icons/fc'
import './project.css'
const Project = () => {
  return (
    <div style={{ display: 'flex' }}>
      <div>
        <Side />
      </div>
      <div>
        <div style={{ display: 'flex', marginLeft: '110px' }}>
          <div style={style.projectback} className='box'>
            <div style={style.icon}>
              <FcAcceptDatabase size={140} />
              <div style={{ marginTop: '40px' }}>
                <i style={style.p1}>Old Projects</i>
              </div>
            </div>
          </div>
          <div style={style.projectback} className='box'>
            <div style={style.icon}>
              <FcRefresh size={140} />
              <div style={{ marginTop: '40px' }}>
                <i style={style.p1}>Current Projects</i>
              </div>
            </div>
          </div>
          <div style={style.projectback} className='box'>
            <div style={style.icon}>
              <FcMindMap size={140} />
              <div style={{ marginTop: '40px' }}>
                <i style={style.p1}>Add Project</i>
              </div>
            </div>
          </div>
        </div>

        <div style={{ display: 'flex', marginLeft: '110px' }}>
          <div style={style.projectback} className='box'>
            <div style={style.icon}>
              <FcFinePrint size={140} />
              <div style={{ marginTop: '40px' }}>
                <i style={style.p1}>Search Projects</i>
              </div>
            </div>
          </div>
          <div style={style.projectback} className='box'>
            <div style={style.icon}>
              <FcBiotech size={140} />
              <div style={{ marginTop: '40px' }}>
                <i style={style.p1}>Edit Projects</i>
              </div>
            </div>
          </div>

          <div style={style.projectback} className='box'>
            <div style={style.icon}>
              <FcFullTrash size={140} />
              <div style={{ marginTop: '40px' }}>
                <i style={style.p1}>Delete Project</i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const style = {
  projectback: {
    borderStyle: 'none',
    height: '300px',
    width: '200px',
    backgroundImage: 'url("projectback2.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    marginTop: '40px',
    marginLeft: '100px',
    padding: '10px',
    textAlign: 'center',
  },
  icon: {
    marginTop: '20px',
    marginLeft: '10px',
  },
  p1: {
    marginTop: '70px',
    fontFamily: 'initial',
    fontSize: '20px',
  },
}

export default Project
