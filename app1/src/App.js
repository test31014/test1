import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Login from './Pages/login'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Adminpage from './Pages/Admin'
import Performance from './Pages/performnace'
import { Doughnut } from 'react-chartjs-2'
import Project from './Pages/project'
import Employee from './Pages/employee'
function App() {
  return (
    <div style={{ height: '100px' }}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Login />} />
          <Route path='/admin' element={<Adminpage />} />
          <Route path='/perfo' element={<Performance />} />
          <Route path='/project' element={<Project />} />
          <Route path='/emp' element={<Employee />} />
        </Routes>
        <ToastContainer position='top-center' autoClose={1000} />
      </BrowserRouter>
      <div></div>
    </div>
  )
}

export default App
