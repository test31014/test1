import './add_emp.css'
import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
const Emp_model = ({ setopen }) => {
  const [email, setemail] = useState('')
  const [role, setrole] = useState('')
  const [department, setdepartment] = useState('')
  const [office, setoffice] = useState('')
  const [project, setproject] = useState('')
  const [ctc, setctc] = useState('')
  const [password, setpassword] = useState('')
  const [join_date, setjoindate] = useState('')

  const add = () => {
    console.log('join date')
    console.log(join_date)

    axios
      .post(
        'http://localhost:4000/addemp',
        { email, role, department, office, project, ctc, password, join_date },
        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        if (response.data.status == 'success') {
          toast.success('New Employee Added')
        } else {
          toast.error('Error While Adding Employee')
        }
      })
  }
  return (
    <div className='modalBackground2'>
      <div className='modalContainer2' style={styles.infoback}>
        <div>
          <h2 style={{ fontFamily: 'initial', color: 'black' }}>
            Add New Eployee
          </h2>
        </div>
        <div className='row'>
          <div className='col'>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Email</label>
              <input
                className='form-control'
                type='email'
                onChange={(e) => {
                  setemail(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Designation</label>
              <input
                className='form-control'
                type='Text'
                onChange={(e) => {
                  setrole(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Department</label>
              <input
                className='form-control'
                type='Text'
                onChange={(e) => {
                  setdepartment(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Join date</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setjoindate(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <div style={{ display: 'flex' }}>
                <button
                  className='btn btn-info'
                  style={{ marginLeft: '120px', marginTop: '15px' }}
                  onClick={add}>
                  Add This Employee
                </button>
              </div>
            </div>
          </div>
          <div className='col'>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Office Location</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setoffice(e.target.value)
                }}
              />
            </div>

            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Project</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setproject(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Cost For Company</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setctc(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Temporary Password</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setpassword(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <div style={{ display: 'flex' }}>
                <button
                  onClick={() => {
                    setopen(false)
                  }}
                  className='btn btn-danger'
                  style={{ marginLeft: '120px', marginTop: '15px' }}>
                  Close This Window
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const styles = {
  p2: {
    fontFamily: 'cursive',
    color: 'white',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '15px',
  },

  infoback: {
    textAlign: 'center',
    backgroundImage: 'url("searchback.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
}

export default Emp_model
