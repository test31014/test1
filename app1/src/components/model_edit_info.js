import './model_edit_info.css'
import { useState } from 'react'

const Model_info = ({ setopen }) => {
  return (
    <div className='modalBackground3'>
      <div className='modalContainer3' style={styles.infoback}>
        <div>
          <h2 style={{ fontFamily: 'initial', color: 'black' }}>
            Edit Information
          </h2>
        </div>
        <div className='row'>
          <div className='col'>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Email</label>
              <input className='form-control' type='email' />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Designation</label>
              <input className='form-control' type='Text' />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Department</label>
              <input className='form-control' type='Text' />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Join date</label>
              <input className='form-control' type='date' />
            </div>
          </div>
          <div className='col'>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Office Location</label>
              <input className='form-control' type='text' />
            </div>

            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Current Project</label>
              <input className='form-control' type='text' />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Employee Id</label>
              <input className='form-control' type='text' />
            </div>
          </div>
        </div>
        <div style={{ display: 'flex' }}>
          <button
            className='btn btn-info'
            style={{ marginLeft: '100px', marginTop: '30px' }}>
            Update Information
          </button>
          <button
            onClick={() => {
              setopen(false)
            }}
            className='btn btn-danger'
            style={{ marginLeft: '100px', marginTop: '30px' }}>
            Close This Window
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  p2: {
    fontFamily: 'cursive',
    color: 'white',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '15px',
  },

  infoback: {
    textAlign: 'center',
    backgroundImage: 'url("infoback.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
}

export default Model_info
