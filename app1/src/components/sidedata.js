import { ImHome } from 'react-icons/im'
import { HiUserAdd } from 'react-icons/hi'
import { DiAtom } from 'react-icons/di'
import { FiActivity } from 'react-icons/fi'
import { FiSend } from 'react-icons/fi'
import { FiAward } from 'react-icons/fi'
import { MdCached } from 'react-icons/md'
export const Sidebardata = [
  {
    title: 'home',
    icon: <ImHome size={28} />,
    link: '/home',
  },
  {
    title: ' Employee Management',
    icon: <HiUserAdd size={28} />,
    link: '/emp',
  },
  {
    title: 'Project',
    icon: <DiAtom size={28} />,
    link: '/project',
  },
  {
    title: 'Performance',
    icon: <FiActivity size={28} />,
    link: '/perfo',
  },
  {
    title: 'Send Message',
    icon: <FiSend size={28} />,
    link: '/home',
  },
  {
    title: 'Profile',
    icon: <FiAward size={28} />,
    link: '/admin',
  },
  {
    title: 'Help',
    icon: <MdCached size={28} />,
    link: '/home',
  },
]
