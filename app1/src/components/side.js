import { FiX } from 'react-icons/fi'
import './side.css'
import { Sidebardata } from './sidedata'
import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'
import axios from 'axios'
const Side = () => {
  const [admin, setadmin] = useState({})
  useEffect(() => {
    const loading = async () => {
      loadadmin()
    }
    loading()
  }, [])

  const loadadmin = async function () {
    await axios
      .get('http://localhost:4000/getadmin', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data
        console.log('axios get')
        console.log(response.data)
        console.log(result.status)
        if (result.status == 'success') {
          setadmin(response.data.data)
          console.log('admin value')
          console.log(admin)
        } else alert('error during getting data')
      })
  }
  const navidate = useNavigate()
  return (
    <div className='sidebar' style={styles.mainbox}>
      <div style={{ background: 'none', marginTop: '0px' }}>
        <div>
          <h6 style={{ color: 'lightblue' }}>Admin</h6>
        </div>
      </div>
      <div
        style={{
          background: 'none',
          marginTop: 'px',
          color: 'lightblue',
        }}>
        <div style={styles.profilebox}>
          <img
            src={'http://localhost:4000/' + admin.image}
            style={{
              width: '100px',
              height: '120px',
              borderRadius: '15px',
            }}
          />
        </div>
        <h6 style={{ marginLeft: '25%', color: '#d4d4dc' }}>
          Employee Id :{admin.emp_id}
        </h6>
        <h6 style={{ marginLeft: '31%', color: ' #3f3f3f' }}>{admin.role}</h6>
      </div>
      <hr></hr>
      <div style={{ marginTop: '30px' }}>
        {Sidebardata.map((data) => {
          return (
            <div
              className='bar'
              style={styles.menu}
              onClick={() => {
                navidate(data.link)
              }}>
              <div style={styles.iconbox}>{data.icon} </div>
              <div style={styles.titlebox}>{data.title} </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

const styles = {
  menu: {
    display: 'flex',
    marginLeft: '0px',
    borderStyle: 'none',
    marginTop: '20px',
    borderStyle: 'none',
  },

  mainbox: {
    backgroundImage: 'url("side4.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },

  iconbox: {
    marginTop: '10px',
    marginLeft: '65px',
    height: '100%',
    width: '30px',
    marginBottom: '10px',
  },

  titlebox: {
    marginTop: '15px',
    fontSize: '15px',
    marginLeft: '20px',
  },
  profilebox: {
    width: '100px',
    height: '100px',
    borderStyle: 'none',
    marginLeft: '30%',
    borderRadius: '15%',
    textAlign: 'center',
    textJustify: 'ceneter',
    marginBottom: '50px',
  },
}
export default Side
