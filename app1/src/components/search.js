import { FaSearch } from 'react-icons/fa'
import { useState } from 'react'
import './search.css'
const employee = [
  {
    name: 'Aniket',
    salary: 5000,
    department: 'Mechanical',
    location: 'pune',
  },
  {
    name: 'Ram',
    salary: 5000,
    department: 'It',
    location: 'Mumbai',
  },
  {
    name: 'Sanket',
    salary: 3000,
    department: 'CS',
    location: 'Kolhapur',
  },
  {
    name: 'Satish',
    salary: 6000,
    department: 'Design',
    location: 'satara',
  },
  {
    name: 'Sayali',
    salary: 5000,
    department: 'It',
    location: 'pune',
  },

  {
    name: 'Pallavi',
    salary: 5000,
    department: 'Design',
    location: 'Mumbai',
  },
  {
    name: 'Rahul',
    salary: 3500,
    department: 'Admin',
    location: 'Rtanagiri',
  },
  {
    name: 'Rana',
    salary: 5000,
    department: 'Mba',
    location: 'Goa',
  },
  {
    name: 'Jay',
    salary: 1500,
    department: 'law',
    location: 'Pune',
  },
  {
    name: 'Mandar',
    salary: 5000,
    department: 'mech',
    location: 'Delhi',
  },
  {
    name: 'Rajesh',
    salary: 5000,
    department: 'banking',
    location: 'Ratnagiri',
  },
]
const Search1 = () => {
  const [query, setquery] = useState('')
  return (
    <div>
      <div>
        <div style={{ textAlign: 'center' }}>
          <div style={{}}>
            <input
              className='input1'
              type='text'
              style={style.input}
              onChange={(e) => {
                setquery(e.target.value)
              }}></input>
          </div>
        </div>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th scope='col'>Name</th>
              <th scope='col'>Location</th>
              <th scope='col'>Salary</th>

              <th scope='col'>Department</th>

              <th scope='col'>Profile</th>
            </tr>
          </thead>
          <tbody>
            {employee
              .filter((emp) => emp.name.toLowerCase().includes(query))
              .map((emp) => {
                return (
                  <tr>
                    <td>{emp.name}</td>
                    <td>{emp.location}</td>
                    <td>{emp.salary}</td>

                    <td>{emp.department}</td>
                    <td>
                      <div style={{ width: '70px' }}></div>
                      <div
                        style={{
                          width: '70px',
                          height: '29px',
                          background: '#96DED1',
                          textAlign: 'center',
                          borderRadius: '10px',
                          border: 'none',
                        }}>
                        Details
                      </div>
                    </td>
                  </tr>
                )
              })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

const style = {
  input: {
    marginTop: '10px',
    background: 'none',
    borderColor: 'black',
    marginBottom: '10px',
    borderRadius: '15px',
  },
}

export default Search1
