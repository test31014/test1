import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Legend,
  Tooltip,
} from 'recharts'

const pdata = [
  {
    name: 'Aniket',
    salary: 2000,
    performance: 7,
    gender: 'male',
  },
  {
    name: 'Jayesh',
    salary: 2500,
    performance: 8.5,
    gender: 'male',
  },
  {
    name: 'Vaibhav',
    salary: 1500,
    performance: 6,
    gender: 'male',
  },
  {
    name: 'Satish',
    salary: 3000,
    performance: 7.5,
    gender: 'male',
  },
  {
    name: 'Sayali',
    salary: 4500,
    performance: 6.2,
    gender: 'female',
  },
  {
    name: 'Neha',
    salary: 3900,
    performance: 9,
    gender: 'female',
  },
  {
    name: 'Rahul',
    salary: 6000,
    performance: 10,
    gender: 'male',
  },
  {
    name: 'Ankita',
    salary: 4200,
    performance: 8.3,
    gender: 'Female',
  },
  {
    name: 'Omkar',
    salary: 3500,
    performance: 6.5,
    gender: 'male',
  },
  {
    name: 'Satish',
    salary: 5000,
    performance: 7.3,
    gender: 'male',
  },
  {
    name: 'Sanket',
    salary: 7800,
    performance: 4.5,
    gender: 'Female',
  },
]
function ChartD() {
  return (
    <div>
      <div
        style={{
          width: '900px',
          height: '800px',
          textAlign: 'center',
          marginTop: '30px',
          fontFamily: 'initial',
        }}>
        <h4 Style={{ textAlign: 'center' }}>Salary Chart</h4>
        <ResponsiveContainer width='100%' aspect={3}>
          <LineChart data={pdata} width={900} height={300}>
            <CartesianGrid />
            <XAxis dataKey='name' interval={'preservStartEnd'} />
            <Line dataKey='salary' stroke='Red' />
            <YAxis />
            <Tooltip contentStyle={{ backgroundColor: 'white' }} />
            <Legend />
          </LineChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default ChartD
