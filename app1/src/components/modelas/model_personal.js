import './model_p.css'
import { useState } from 'react'
import { toast } from 'react-toastify'
import axios from 'axios'
const Model_personal = ({ setopen }) => {
  const [name, setname] = useState('')
  const [DOB, setdob] = useState('')
  const [contact, setcontact] = useState('')
  const [address, setaddress] = useState('')
  const [emergency, setem] = useState('')
  const [blood, setblood] = useState('')
  const [education, setedu] = useState('')
  const [adhar_no, setadhar] = useState('')

  const update = () => {
    axios
      .put(
        'http://localhost:4000/uploadi',
        { name, DOB, contact, address, emergency, blood, education },
        {
          headers: {
            token: sessionStorage['token'],
          },
        }
      )
      .then((reaponse) => {
        if (reaponse.data.status === 'success') {
          toast.success('Information Up-Dated ')
          window.location.reload()
        } else {
          toast.error(' Error Information Up-Dated ')
        }
      })
  }
  return (
    <div className='modalBackground5'>
      <div className='modalContainer5' style={styles.infoback}>
        <div>
          <h2 style={{ fontFamily: 'initial', color: 'black' }}>
            Edit Information
          </h2>
        </div>
        <div className='row'>
          <div className='col'>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Name</label>
              <input
                className='form-control'
                type='email'
                onChange={(e) => {
                  setname(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Date of Birth</label>
              <input
                className='form-control'
                type='Text'
                onChange={(e) => {
                  setdob(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Conatct</label>
              <input
                className='form-control'
                type='Text'
                onChange={(e) => {
                  setcontact(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Education</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setedu(e.target.value)
                }}
              />
            </div>
          </div>
          <div className='col'>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Address</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setaddress(e.target.value)
                }}
              />
            </div>

            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Emergency Contact</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setem(e.target.value)
                }}
              />
            </div>
            <div className='mb-3' style={{ width: '90%', textAlign: 'left' }}>
              <label>Blood Group</label>
              <input
                className='form-control'
                type='text'
                onChange={(e) => {
                  setblood(e.target.value)
                }}
              />
            </div>
          </div>
        </div>
        <div style={{ display: 'flex' }}>
          <button
            className='btn btn-info'
            style={{ marginLeft: '100px', marginTop: '30px' }}
            onClick={update}>
            Update Information
          </button>
          <button
            onClick={() => {
              setopen(false)
            }}
            className='btn btn-danger'
            style={{ marginLeft: '100px', marginTop: '30px' }}>
            Close This Window
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  p2: {
    fontFamily: 'cursive',
    color: 'white',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '15px',
  },

  infoback: {
    textAlign: 'center',
    backgroundImage: 'url("infoback.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
}

export default Model_personal
