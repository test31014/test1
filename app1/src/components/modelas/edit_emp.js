import './remove_emp.css'
import { useState } from 'react'

const Edit_emp = ({ setopen }) => {
  return (
    <div className='modalBackground4'>
      <div className='modalContainer4' style={styles.infoback}>
        <div>
          <h2 style={{ fontFamily: 'initial', color: 'black' }}>
            Edit Eployee
          </h2>
        </div>
        <div style={{ textAlign: 'center' }}>
          <div className='mb-3' style={{ width: '90%', textAlign: 'center' }}>
            <div>
              <div
                className='mb-3'
                style={{ width: '40%', textAlign: 'left', marginLeft: '35%' }}>
                <label>Employee Id</label>
                <input className='form-control' type='email' />
              </div>
              <div
                className='mb-3'
                style={{ width: '40%', textAlign: 'left', marginLeft: '35%' }}>
                <label>Department</label>
                <input className='form-control' type='email' />
              </div>
              <div
                className='mb-3'
                style={{ width: '40%', textAlign: 'left', marginLeft: '35%' }}>
                <label>Name of Employee</label>
                <input className='form-control' type='email' />
              </div>
            </div>
            <div style={{ display: 'flex' }}>
              <button
                className='btn btn-info'
                style={{ marginLeft: '35%', marginTop: '30px' }}>
                Edit Employee
              </button>
              <button
                onClick={() => {
                  setopen(false)
                }}
                className='btn btn-danger'
                style={{ marginLeft: '70px', marginTop: '30px' }}>
                Close Window
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const styles = {
  p2: {
    fontFamily: 'cursive',
    color: 'white',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '15px',
  },

  infoback: {
    textAlign: 'center',
    backgroundImage: 'url("searchback.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
}

export default Edit_emp
