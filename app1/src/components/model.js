import React from 'react'
import './model.css'
import './button.css'

const Model = ({ setopen }) => {
  return (
    <div className='modalBackground'>
      <div className='modalContainer' style={{ textAlign: 'center' }}>
        <div>
          <h2 style={{ fontFamily: 'cursive', color: 'pink' }}>
            We Are Here To Help
          </h2>
        </div>
        <div style={{ marginTop: '40px' }}>
          <button className='btn-hover color-1'>
            {' '}
            How to Login as employee
          </button>
        </div>
        <div style={{ marginTop: '0px' }}>
          <button className='btn-hover color-2'>
            I can't Login into Portal
          </button>
        </div>
        <div style={{ marginTop: '0px' }}>
          <button className='btn-hover color-3'>forgot password</button>
        </div>
        <div style={{ marginTop: '0px' }}>
          <button className='btn-hover color-4'>Contact Us</button>
        </div>
        <div>
          <button
            onClick={() => {
              setopen(false)
            }}
            className='btn btn-danger'
            style={{ marginLeft: '380px', marginTop: '30px' }}>
            close
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  p2: {
    fontFamily: 'cursive',
    color: 'white',
    forntWeight: 'bold',
    fontWeight: 'bold',
    fontSize: '15px',
  },
}

export default Model
