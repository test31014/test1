import { useSpring, useSpringRef } from 'react-spring'
import { useChain, animated, useTransition } from 'react-spring'

const Head = ({ p }) => {
  // Build a spring and catch its ref
  const values = ['aniket']
  const springRef = useSpringRef()
  const props = useSpring({ ...values, ref: springRef })
  // Build a transition and catch its ref
  const transitionRef = useSpringRef()
  const transitions = useTransition({ ...values, ref: transitionRef })
  // First run the spring, when it concludes run the transition
  useChain([springRef, transitionRef])
  // Use the animated props like always
  return (
    <animated.div style={props}>
      {transitions((styles) => (
        <animated.div style={styles} />
      ))}
    </animated.div>
  )
}

export default Head
